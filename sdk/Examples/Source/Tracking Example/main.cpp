//
//  main.cpp
//  ARToolKit6 Tracking Example
//
//  Copyright 2015-2016 Daqri LLC. All Rights Reserved.
//  Copyright 2008-2015 ARToolworks, Inc. All Rights Reserved.
//
//  Author(s): Daniel Bell, Philip Lamb
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#ifdef DEBUG
#  ifdef _WIN32
#    define MAXPATHLEN MAX_PATH
#    include <direct.h>               // _getcwd()
#    define getcwd _getcwd
#  else
#    include <unistd.h>
#    include <sys/param.h>
#  endif
#endif
#include <unistd.h>

#include <AR6/ARController.h>

#include <SDL2/SDL.h>

const char *vconf = NULL;

static ARController* arController = NULL;

struct trackable {
    const char *name;
    float height;
};
static const struct trackable trackables[] = {
    {"Alterra_Ticket_1.jpg", 95.3},
    {"Alterra_Postcard_2.jpg", 95.3},
    {"Alterra_Postcard_3.jpg", 127.0},
    {"Alterra_Postcard_4.jpg", 95.3}
};
static const int trackableCount = (sizeof(trackables)/sizeof(trackables[0]));

static void quit(int rc)
{
    if (arController) {
        arController->displayFrameFinal(0);
        arController->shutdown();
        delete arController;
    }
    SDL_Quit();
    exit(rc);
}

int main(int argc, char *argv[])
{
    
    // Initialize SDL.
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        ARLOGe("Error: SDL initialisation failed. SDL error: '%s'.\n", SDL_GetError());
        quit(1);
        return 1;
    }
   
    // Initialise the ARController.
    arController = new ARController();

    if (!arController->initialiseBase()) {
        ARLOGe("Error initialising ARController.\n");
        quit(-1);
    }
    
    
#ifdef DEBUG
    arLogLevel = AR_LOG_LEVEL_DEBUG;
#endif
    
    // Add trackables.
    int trackableIDs[trackableCount];
#ifdef DEBUG
    char buf[MAXPATHLEN];
    ARLOGd("CWD is '%s'.\n", getcwd(buf, sizeof(buf)));
#endif
    char *resourcesDir = arUtilGetResourcesDirectoryPath(AR_UTIL_RESOURCES_DIRECTORY_BEHAVIOR_BEST);
    char *trackableConfig;
    for (int i = 0; i < trackableCount; i++) {
        asprintf(&trackableConfig, "2d;%s/%s;%f", resourcesDir, trackables[i].name, trackables[i].height);
        trackableIDs[i] = arController->addTrackable(trackableConfig);
        if (trackableIDs[i] == -1) {
            ARLOGe("Error adding trackable.\n");
            quit(-1);
        }
        free(trackableConfig);
    }
    arController->get2DTracker().setMaxSimultaneousTrackedImages(trackableCount);

#ifdef DEBUG
    ARLOGd("vconf is '%s'.\n", vconf);
#endif

    // Start tracking.
    arController->startRunning(vconf, NULL, NULL, 0);
    
    free(resourcesDir);

    // Main loop.
    bool done = false;
    while (!done) {
        
        SDL_Event ev;
        while (SDL_PollEvent(&ev)) {
         }

        bool gotFrame = arController->capture();
        if (!gotFrame) {
            usleep(1000);
        } else {
            if (!arController->update()) {
                ARLOGe("Error in ARController::update().\n");
                quit(-1);
            }

            if (!arController->save2DTrackerImageDatabase("/Users/ryohey/Desktop/recognizer.dat")) {
                ARLOGe("Error in ARController::save2DTrackerImageDatabase().\n");
                quit(-1);
            }
            break;
        } //end: if (gotFrame)
    } //end: while (!done) {
    
    quit(0);
    return 0;
}
